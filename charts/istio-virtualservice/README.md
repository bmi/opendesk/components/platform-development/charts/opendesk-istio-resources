<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# istio-virtualservice

A Helm chart for deploying an istio VirtualService

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-istio-resources https://gitlab.opencode.de/api/v4/projects/1386/packages/helm/stable
helm install my-release --version 2.0.2 opendesk-istio-resources/istio-virtualservice
```

### Install via OCI Registry

```console
helm repo add opendesk-istio-resources oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources
helm install my-release --version 2.0.2 opendesk-istio-resources/istio-virtualservice
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| annotations | object | `{}` | Define custom VirtualService annotations. |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| gatewayName | string | `"istio-gateway"` | Define a corresponding already existing gateway. |
| host | string | `"service.example.com"` | Define the host/domain name for Gateway. |
| http | list | `[]` | VirtualService http settings.  Ref.: https://istio.io/latest/docs/reference/config/networking/virtual-service/ |
| nameOverride | string | `""` | String to partially override release name. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
